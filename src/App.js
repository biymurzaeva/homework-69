import './App.css';
import Layout from "./components/Layout/Layout";
import {Container} from "@material-ui/core";
import {Switch} from "react-router-dom";
import SearchForm from "./components/SearchForm/SearchForm";
import {Route} from "react-router-dom";
import TvShowInfo from "./containers/TVShowInfo/TVShowInfo";
import React from "react";

const App = () => (
  <Layout>
    <Container>
      <SearchForm/>
      <Switch>
        <Route path="/" exact render={() => <h2>Welcome</h2>}/>
        <Route path="/shows/:id"  component={TvShowInfo}/>
      </Switch>
    </Container>
  </Layout>
);

export default App;
