import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchData, fetchShow} from "../../store/actions";
import {Autocomplete} from "@material-ui/lab";
import {TextField} from "@material-ui/core";
import './SearchForm.css';
import {useHistory} from "react-router-dom";

const SearchForm = () => {
	const history = useHistory();

	const dispatch = useDispatch();
	const data = useSelector(state => state.data);

	useEffect( () => {
		dispatch(fetchData());
	}, [dispatch]);

	const getShowInfo = async id => {
		await dispatch(fetchShow(id));
		history.push(`/shows/${id}`);
	};

	return (
		<div className="SearchForm">
			<p>Search for TV Shows: </p>
			<Autocomplete
				id="tv-shows"
				options={data}
				getOptionLabel={option => option.show.name}
				onChange={(e, showId) => getShowInfo(showId.show.id)}
				style={{ width: 300 }}
				renderInput={params =>
					<TextField
						{...params}
						label="TV Shows"
						variant="outlined"
					/>
				}
			/>
		</div>
	);
};

export default SearchForm;