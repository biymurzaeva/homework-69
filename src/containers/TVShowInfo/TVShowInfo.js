import React from 'react';
import {useSelector} from "react-redux";
import './TVShoowInfo.css';

const TvShowInfo = () => {
	const show = useSelector(state => state.show);
	const description = show.summary;

	return show && (
		<div className="Show">
			<div className="ImgBlock">
				<img src={show.image['medium']} alt={show.name}/>
			</div>
			<div className="DescriptionBlock">
				<h3>{show.name}</h3>
				<p dangerouslySetInnerHTML={{__html: description}}/>
			</div>
		</div>
	);
};

export default TvShowInfo;