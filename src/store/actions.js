import axios from "axios";

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const FETCH_SHOW_REQUEST = 'FETCH_SHOW_REQUEST';
export const FETCH_SHOW_SUCCESS = 'FETCH_SHOW_SUCCESS';
export const FETCH_SHOW_FAILURE = 'FETCH_SHOW_FAILURE';

export const fetchShowRequest = () => ({type: FETCH_SHOW_REQUEST});
export const fetchShowSuccess = show => ({type: FETCH_SHOW_SUCCESS, payload: show});
export const fetchShowFailure = () => ({type: FETCH_SHOW_FAILURE});

export const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
export const fetchDataSuccess = shows => ({type: FETCH_DATA_SUCCESS, payload: shows});
export const fetchDataFailure = () => ({type: FETCH_DATA_FAILURE});

export const fetchData = () => {
	return async (dispatch) => {
		dispatch(fetchDataRequest());

		try {
			const response = await axios.get('http://api.tvmaze.com/search/shows?q=csi');

			if (response.data === null) {
				dispatch(fetchDataSuccess(''));
			} else {
				dispatch(fetchDataSuccess(response.data));
			}

		} catch (e) {
			dispatch(fetchDataFailure());
		}
	}
}

export const fetchShow = id => {
	return async (dispatch) => {
		dispatch(fetchShowRequest());

		try {
			const response = await axios.get(`https://api.tvmaze.com/shows/${id}`);

			if (response.data === null) {
				dispatch(fetchShowSuccess({}));
			} else {
				dispatch(fetchShowSuccess(response.data));
			}

		} catch (e) {
			dispatch(fetchShowFailure());
		}
	}
}