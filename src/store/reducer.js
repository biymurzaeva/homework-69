import {
	FETCH_DATA_FAILURE,
	FETCH_DATA_REQUEST,
	FETCH_DATA_SUCCESS, FETCH_SHOW_FAILURE,
	FETCH_SHOW_REQUEST,
	FETCH_SHOW_SUCCESS
} from "./actions";

const initialState = {
	data: [],
	show: {},
	loading: false,
	error: null,
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_DATA_REQUEST:
			return {...state, error: null, loading: true};
		case FETCH_DATA_SUCCESS:
			return {...state, loading: false, data: action.payload};
		case FETCH_DATA_FAILURE:
			return {...state, loading: false, error: action.payload};
		case FETCH_SHOW_REQUEST:
			return {...state, error: null, loading: true};
		case FETCH_SHOW_SUCCESS:
			return {...state, loading: false, show: action.payload};
		case FETCH_SHOW_FAILURE:
			return {...state, loading: false, error: action.payload};
		default:
			return state;
	}
};

export default reducer;

